import logging as log
import json
import random as rnd


ver = 0.4
logparam = True
fileLog = "cmd2try.log"
LOG_FORMAT = "%(levelname)s %(asctime)s: %(message)s"
log.basicConfig(filename=fileLog, level=0, format=LOG_FORMAT, filemode='w')
logw = log.getLogger()
# logger test (if programm errors when running uncomment next string)
# logger.info("Test message 1")

with open("ru.json") as file1:
    rootusers = json.load(file1)
with open("SLAP.json") as file2:
    users = json.load(file2)

# users =
root = False
id_ = rnd.randint(0, 9999)
cmdlist = ['version',
          'root',
          'exit',
          'help',
          'users',
          'rru',
          'yes or no',
          'SLAP']
cmdlistuse = ['Check version',
             'Are you rooted',
             'Exit',
             'Print this list',
             'Print all users',
             'Register Rooted User',
             'Desicion maker',
             'Save Logins And Passwords']
cmdict = dict(zip(cmdlist, cmdlistuse))

def register():
    loginCreate = input('Create login: ')
    if loginCreate in users:
        print('Login already exist')
        register()
    else:
        passwordCreate = input('Create password: ')
        while len(passwordCreate) < 8:
            if logparam:
                logw.warning('Password is less than 8 characters')
            print('Your password needs to be atleast 8 characters long')
            passwordCreate = input('Create password: ')
        while passwordCreate == loginCreate:
            print('Your password can\'t be the same as your login')
            passwordCreate = input('Create password: ')
        logw.info('New user created, login is {0}, password is {1}'.format(loginCreate, passwordCreate))

        users[loginCreate] = passwordCreate
        login()


def rru():
    loginCreate = input('Create login: ')
    if loginCreate in users:
        print('Login already exist')
        register()
    else:
        passwordCreate = input('Create password: ')
        while len(passwordCreate) < 8:
            logw.warning('Password is less than 8 characters')
            print('Your password needs to be atleast 8 characters long')
            passwordCreate = input('Create password: ')
        while passwordCreate == loginCreate:
            print('Your password can\'t be the same as your login')
            passwordCreate = input('Create password: ')
        logw.info('New rooted user created, pass is ' + passwordCreate + ', login is ' + loginCreate)
        users[loginCreate] = passwordCreate
        rootusers[loginCreate] = True


def login():
    global root
    loginName = input('Login: ')
    loginPass = input('Password: ')
    if loginName in users and users[loginName] == loginPass:
        if logparam:
            logw.info('User logged in as ' + loginName)
        try:
            if rootusers[loginName]:
                root = True
        except KeyError:
            root = False
        return loginName
    else:
        print('Login or password is incorrect')
        if logparam:
            logw.info('Login or password is incorrect')
        login()


def log_nacommand(na_command):
    print('You has no access to this command, this will be reported in the log')
    logw.warning('User tried to access command: ' + na_command)


def lcmd(com):
    if logparam:
        logw.info('User tried to execute: ' + com)


def mainmenu():
    answer = input('Are you registered? (y/n/exit): ')
    while answer != 'y' or answer != 'n':
        if answer.lower() == 'y':
            login()
            break
        elif answer.lower() == 'n':
            register()
            break
        elif answer == 'exit':
            exit(0)
        elif answer != 'y' or answer != 'n' or answer != 'exit':
            answer = input('Are you registered? (y/n): ')


mainmenu()
# print('Rooted: ' + str(root))


while 1:
    command = input('!> ')
    if command == 'version':
        print(ver)
    elif command == 'root':
        print(str(root))
        lcmd(command)
    elif command == 'exit':
        lcmd('exitting')
        mainmenu()
    elif command == 'users' and root:
        for key, value in users.items():
            print('Username:', key, 'Password:', value)
        lcmd(command)
    elif command == 'users' and root is False:
        log_nacommand(command)
    elif command == 'rru':
        rru()
        lcmd(command)
    elif command == 'yes or no':
        rd_des = rnd.randint(0, 1)
        print(('yes' if rd_des else 'no'))
        lcmd(command)
    elif command == 'SLAP' and root:
        print('Saving...')
        with open('ru.json', 'w') as file1:
            file1.write(json.dumps(rootusers, indent=2))
        with open('SLAP.json', 'w') as file2:
            file2.write(json.dumps(users, indent=2))
        lcmd(command)
    elif command == 'passwd':
        print('Currently unavailable')
    elif command == 'help':
        for o, v in cmdict.items():
            print(o, '-', v)
    else:
        print('Invalid command')
